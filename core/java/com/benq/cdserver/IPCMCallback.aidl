package  com.benq.cdserver;

interface IPCMCallback {
    void actionPerformed(int actionId);
    void passString(String szText);
}
