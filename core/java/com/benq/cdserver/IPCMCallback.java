/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /media/SECONDARY/BenQ/CloudDisplay/CdServer/src/com/benq/cdserver/IPCMCallback.aidl
 */
package com.benq.cdserver;
public interface IPCMCallback extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements com.benq.cdserver.IPCMCallback
{
private static final java.lang.String DESCRIPTOR = "com.benq.cdserver.IPCMCallback";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an com.benq.cdserver.IPCMCallback interface,
 * generating a proxy if needed.
 */
public static com.benq.cdserver.IPCMCallback asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = (android.os.IInterface)obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof com.benq.cdserver.IPCMCallback))) {
return ((com.benq.cdserver.IPCMCallback)iin);
}
return new com.benq.cdserver.IPCMCallback.Stub.Proxy(obj);
}
public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_actionPerformed:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
this.actionPerformed(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_passString:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
this.passString(_arg0);
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements com.benq.cdserver.IPCMCallback
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
public void actionPerformed(int actionId) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(actionId);
mRemote.transact(Stub.TRANSACTION_actionPerformed, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void passString(java.lang.String szText) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(szText);
mRemote.transact(Stub.TRANSACTION_passString, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_actionPerformed = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_passString = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
}
public void actionPerformed(int actionId) throws android.os.RemoteException;
public void passString(java.lang.String szText) throws android.os.RemoteException;
}
