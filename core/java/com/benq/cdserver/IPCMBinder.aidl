package  com.benq.cdserver;

import com.android.internal.view.IPCMCallback;

interface IPCMBinder {
    int getPid();
    void registerCallback(IPCMCallback cb);
    void unregisterCallback(IPCMCallback cb);
     
}
